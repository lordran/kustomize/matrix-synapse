#!/bin/sh

work_dir=$(dirname $0)

pass perso/Servers/Synapse/matrix.lordran.net.signing.key > $work_dir/matrix.lordran.net.signing.key
pass perso/Servers/Synapse/matrix.lordran.net.macaroon.key > $work_dir/matrix.lordran.net.macaroon.key
pass perso/Servers/Synapse/matrix.lordran.net.registration.key > $work_dir/matrix.lordran.net.registration.key

MACAROON_SECRET_KEY=$(cat $work_dir/matrix.lordran.net.macaroon.key) \
    REGISTRATION_SHARED_SECRET=$(cat $work_dir/matrix.lordran.net.registration.key) \
    POSTGRESQL_PASSWORD=$(pass perso/Servers/Synapse/pgsql) \
    envsubst > $work_dir/homeserver.yaml < $work_dir/homeserver.yaml.tmpl
